package com.web.tests;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.web.base.Service;
import com.web.pages.AmazonAddtocart;
import com.web.pages.AmazonHomepage;
import com.web.pages.AmazonSearchpage;
import com.web.pages.AmazonSelectProduct;
import com.web.pages.Page1;
import com.web.pages.Signinpage;

public class Amazonsecondscenario {
	
	WebDriver driver=null;	
	
	@BeforeTest	
	public void beforeTest(){
	driver=new Service().initializeDriver("chrome");	
	driver.manage().window().maximize();
	driver.manage().deleteAllCookies();
	}
	
	@Test
	public void test() throws InterruptedException, IOException{
	Page1 one=new Page1(this.driver);
	AmazonHomepage amazon=new AmazonHomepage(this.driver);
	Signinpage signin=new Signinpage(this.driver);
	AmazonSearchpage searchpage = new AmazonSearchpage(driver);
	AmazonSelectProduct selectproduct= new AmazonSelectProduct(driver);
	AmazonAddtocart addcart =new AmazonAddtocart(this.driver);
		
	driver.get("https://www.google.com");
	
	amazon.performSearch();
	amazon.clickOnAmazon();	
	searchpage.searchProduct();
	
	selectproduct.selectProduct();
	
	addcart.addtoCart();
	
	addcart.clickOnCancel();
	addcart.clickOnSigin();
	
   // amazon.clickOnSignin();
	
	signin.enterEmailAddress();
	signin.clickOnContinue();
	signin.enterPassword();
	signin.clickOnSigin();

	}
	
	@AfterTest	
	public void terminateBrowser(){
	driver.close();
	driver.quit();
	}
}
