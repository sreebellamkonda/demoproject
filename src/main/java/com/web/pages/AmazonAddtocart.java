package com.web.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.web.base.Resuable;

public class AmazonAddtocart extends Resuable {

	public WebDriver driver = null;

	public AmazonAddtocart(WebDriver driver) {
		super(driver);
		this.driver = driver;
	}

	By Locator = By.name("submit.add-to-cart");
	By Locator2 = By.xpath("//a[contains(@class,'close-button')]");
	By Locator3 = By.xpath("//a[@id='nav-link-accountList']");

	public void addtoCart() throws InterruptedException {
		//validatePresenceofWindowHandle();
		windowHandles();
		elementToBePresent(Locator);
		WebElement addCart = driver.findElement(Locator);
		highlightElement(addCart, 10);
		performClick(addCart, 5);
	}

	public void clickOnCancel() throws InterruptedException {
		//windowHandles();	
		//Thread.sleep(4000);
		elementToBePresent(Locator2);
		WebElement clickXbutton = driver.findElement(Locator2);
		//highlightElement(clickXbutton, 5);
		performClick(clickXbutton,5);
	}

	public void clickOnSigin() {
		elementToBePresent(Locator3);
		WebElement clickSigin = driver.findElement(Locator3);
		//performClick(clickSigin,5);
		mouseHoveronElement(clickSigin,5);
	}

}
