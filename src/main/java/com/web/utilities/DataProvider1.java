package com.web.utilities;

import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;

import org.testng.annotations.DataProvider;
import com.sun.org.apache.bcel.internal.classfile.Method;

public class DataProvider1 {

	static HashMap<String, String> tcAttributeMp = null;

	@DataProvider(name = "getTestData")
	public static Object[][] fileData(Method M) throws IOException {
		String dataProvider, testName;
		dataProvider = "testInformation.xlsx";
		testName = M.getName();
		Object[][] testData = ExcelReader.getDataValues(dataProvider, "Sheet1", testName);

		HashMap<String, String> tcAttribMap = null;
		Object[][] testOutput = new Object[testData.length - 1][1];
		for (int i = 1; i < testData.length; i++) {
			tcAttributeMp = new HashMap<String, String>();
			for (int j = 0; j < testData[i].length; j++) {
				if (testData[i][j] == null) {
					tcAttribMap.put(testData[0][j].toString(), "");
				} else
				// need to fix
				{
					tcAttribMap.put(testData[0][j].toString(), testData[i][j].toString());
				}

				testOutput[i - 1][0] = tcAttribMap;
				tcAttribMap = null;
			}
		}

		return testOutput;

	}
}
