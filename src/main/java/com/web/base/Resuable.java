package com.web.base;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Resuable extends VerificationMethods {
	WebDriver driver = null;
	JavascriptExecutor js = null;
	WebDriverWait wait = null;// declared globally overiridning constructor

	public Resuable(WebDriver driver) {
		super(driver);
		this.driver = driver;
		wait = new WebDriverWait(this.driver, 30);
		js = (JavascriptExecutor) this.driver;
	}

	// performclick
	public void performClick(WebElement element) {
		waitForElementtobeClickable(element);
		element.click();
	}

   //performclick userdefined time
	public void performClick(WebElement element, int time) { // implementing polymorphism method overriding
		waitForElementtobeClickable(element, time);
		element.click();
	}

//wait for element to be clickable
	public void waitForElementtobeClickable(WebElement element, int time) {
		new WebDriverWait(driver, time).until(ExpectedConditions.elementToBeClickable(element));
	}

//wait for element to be clickable
	public void waitForElementtobeClickable(WebElement element) {
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
//-------------------------------------------------------------------------------------------------------------------//
//insert text
	public void insertText(WebElement element, String value) {
		waitForElementtobeClickable(element);
		element.clear();
		element.sendKeys(value);
	}

	public void insertText(WebElement element, String value, int time) {
		waitForElementtobeClickable(element, time);
			element.clear();
		element.sendKeys(value);
	}
	
//-------------------------------------------------------------------------------------------------------------------------//	

//javascript insert text 
	public void insertTextJavascript(WebElement element, String value) {
		waitForElementtobeClickable(element);
			js.executeScript("arguments[0].value='" + value + "';", element);
	}

	public void insertTextJavascript(WebElement element, String value, int time) {
		waitForElementtobeClickable(element, time);
			js.executeScript("arguments[0].value='" + value + "';", element);
	}
	
	//-------------------------------------------------------------------------------------------------------------------------//	
	
//javascript click
	public void performClickJavascript(WebElement element) {
		waitForElementtobeClickable(element);
			//wait.until(ExpectedConditions.elementToBeClickable(element));
		js.executeScript("arguments[0].click();", element);
	}

	public void performClickJavascript(WebElement element, int time) {
		waitForElementtobeClickable(element, time);
		js.executeScript("arguments[0].click();", element);
	}
	//-------------------------------------------------------------------------------------------------------------------------//	

//gettext
	public String getTxt(WebElement element) {
		waitForElementtobeClickable(element);
		//element.clear();
		System.out.println(element.getText());
		return element.getText();
		
	}

	public String getTxt(WebElement element, int time) {
		waitForElementtobeClickable(element, time);
		System.out.println(element.getText());		
		return element.getText();
	}
	//-------------------------------------------------------------------------------------------------------------------------//	

	// getattribute
	public String getAttribute(WebElement element, String value) {

		wait.until(ExpectedConditions.elementToBeSelected(element));
		//element.clear();
		return element.getAttribute(value);
	}

	public String getAttribute(WebElement element, String value, int time) {

		new WebDriverWait(driver, time).until(ExpectedConditions.elementToBeClickable(element));
		//element.clear();
		return element.getAttribute(value);
	}
	//-------------------------------------------------------------------------------------------------------------------------//	

//select dropdown_visibletext
	public void selectDropdown_visibleText(WebElement element, String value) {
		wait.until(ExpectedConditions.elementToBeSelected(element));
		Select dropdown = new Select(element);
		dropdown.selectByVisibleText(value);

	}

	public void selectDropdown_visibleText(WebElement element, String value, int time) {
		new WebDriverWait(driver, time).until(ExpectedConditions.elementToBeSelected(element));
		Select dropdown = new Select(element);
		dropdown.selectByVisibleText(value);

	}
	//-------------------------------------------------------------------------------------------------------------------------//	

//selectwait dropdown_indexvalue
	public void selectDropdown_visibleText(WebElement element, int value) {
		wait.until(ExpectedConditions.elementToBeSelected(element));
		Select dropdown = new Select(element);
		dropdown.selectByIndex(value);

	}

	public void selectDropdown_visibleText(WebElement element, int value, int time) {
		new WebDriverWait(driver, time).until(ExpectedConditions.elementToBeSelected(element));
		Select dropdown = new Select(element);
		dropdown.selectByIndex(value);

	}
	//-------------------------------------------------------------------------------------------------------------------------//	

//Element to be visible in DOM of page
	public void elementToBePresent(By Locator) {
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(Locator));

	}

	public void elementToBePresent(By Locator, int time) {

		new WebDriverWait(driver, time).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(Locator));
	}
	//-------------------------------------------------------------------------------------------------------------------------//	

	public void gettextAlert() {
		wait.until(ExpectedConditions.alertIsPresent());
		driver.switchTo().alert().getText();
	}

	public void gettextAlert(int time) {
		new WebDriverWait(driver, time).until(ExpectedConditions.alertIsPresent());
		driver.switchTo().alert().getText();
	}
	//-------------------------------------------------------------------------------------------------------------------------//	

	public void acceptAlert() {
		wait.until(ExpectedConditions.alertIsPresent());
		driver.switchTo().alert().accept();
	}

	public void acceptAlert(int time) {
		new WebDriverWait(driver, time).until(ExpectedConditions.alertIsPresent());
		driver.switchTo().alert().accept();
	}
	//-------------------------------------------------------------------------------------------------------------------------//	

	public void dismissAlert() {
		wait.until(ExpectedConditions.alertIsPresent());
		driver.switchTo().alert().dismiss();
	}

	public void dismissAlert(int time) {
		new WebDriverWait(driver, time).until(ExpectedConditions.alertIsPresent());
		driver.switchTo().alert().dismiss();
	}
	//-------------------------------------------------------------------------------------------------------------------------//	

	// Switch to Frame
	public void switchFrame(WebElement element) {
		waitForFrameAvailability(element);
		driver.switchTo().frame(element);
	}

	// switch to frame with userdefined time
	public void switchFrame(WebElement element, int time) {
		waitForFrameAvailability(element, time);
		driver.switchTo().frame(element);
	}

	// wait for frame to be availble without time
	public void waitForFrameAvailability(WebElement element) {
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(element));

	}
	// wait for frame to be availble with userdefined time

	public void waitForFrameAvailability(WebElement element, int time) {
		new WebDriverWait(driver, time).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(element));
	}
	//-------------------------------------------------------------------------------------------------------------------------//	
	//insert text
		public void keysReturn(WebElement element) {
			waitForElementtobeClickable(element);
			element.sendKeys(Keys.RETURN);
		}

		public void keysReturn(WebElement element, int time) {
			waitForElementtobeClickable(element, time);
								element.sendKeys(Keys.RETURN);
		}
	//-------------------------------------------------------------------------------------------------------------------------//	
		public void windowHandles() throws InterruptedException {
				
		String winHandleBefore = driver.getWindowHandle();
		//wait.until(ExpectedConditions.numberOfWindowsToBe(1));
		// Perform the click operation that opens new window
         //Thread.sleep(4000);
		// Switch to new window opened
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		    
		}
		}
		
		//-------------------------------------------------------------------------------------------------------------------------//	
	
		public void highlightElement(WebElement element,int time) {
			waitForElementtobeClickable(element, time);
			js.executeScript("arguments[0].setAttribute('style', 'background: groove green; border: 100px groove green;');", element);
		}
	

			
		
//-------------------------------------------------------------------------------------------------------------------------//	

public void mouseHoveronElement(WebElement element,int time) {


Actions action = new Actions(driver);
waitForElementtobeClickable(element,time);
action.moveToElement(element).build().perform();

}
}

// scroll to leement--java code , actions

	// drag and drop


